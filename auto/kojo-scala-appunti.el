(TeX-add-style-hook
 "kojo-scala-appunti"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "italian") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "babel"
    "fontenc"
    "fontspec"
    "graphicx"
    "mathtools"
    "algpseudocode"
    "algorithm"
    "listings"
    "minted"
    "hyperref"
    "ulem"
    "tikz"
    "lastpage"
    "fancyhdr"
    "geometry"
    "enumitem")
   (TeX-add-symbols
    "dontdofcolorbox")
   (LaTeX-add-labels
    "sec:org7454dc3"
    "org998fca1"
    "sec:org5759aa9"
    "sec:orgb831c1d"
    "sec:org3cdef44"
    "sec:org060eaed"
    "sec:org8f817ec"
    "sec:orgfddfc19"
    "sec:orgc220eff"
    "sec:org29f42fd"
    "sec:org50c4951"
    "sec:orgdd815ee"
    "sec:org29f561d"
    "sec:orgb617289"
    "sec:org7203a3e"
    "sec:org22ae432"
    "sec:org5cd3e08"
    "sec:org190d06d"
    "sec:orgb31d3e5"
    "sec:orge5df7a9"
    "sec:org1a33809"
    "sec:orgb3be96e"
    "sec:org79f4796"
    "sec:orgc17d5d9"
    "sec:orgd507f21"
    "sec:org5955f21"
    "sec:orgce78c02"))
 :latex)

