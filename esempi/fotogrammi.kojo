/* -*- Scala -*- */

pulisci()
invisibile()

showAxes()

val tarta = nuovaTartaruga(0, 0)

def quadrato(tarta: Tartaruga) {
  tarta.ritardo(0)
  tarta.coloreRiempimento(verde)
  ripeti(4) {
    tarta.avanti(50)
    tarta.destra(90)
  }
  tarta.destra(90)
  pause(0.05)
}

/*
 var fotogramma = 1
 ripeti(10) {
 tarta.pulisci()
 tarta.invisibile()
 tarta.saltaVerso(4 * fotogramma, 0)
 quadrato(tarta)
 fotogramma = fotogramma + 1
 }
 */

ripetiPerOgniElementoDi(1 to 50) { fotogramma =>
  tarta.pulisci()
  //tarta.invisibile()
  tarta.saltaVerso(4 * fotogramma, 0)
  quadrato(tarta)
}
