/*
; Eye by Paolo Passaro

repeat 1800 [fd ln repcount bk 10*sin repcount rt 10]
*/

pulisci
ritardo(10)

ripetizione(1800) { i =>
  avanti(Math.log(i))
  indietro(10 * Math.sin(i))
  destra(10)
}

