/*
; Head program by Keith Enevoldsen
; from http://www.mathcats.com/gallery/15wordcontest.html

repeat 38 [fd 5 rt 10]
setx 55
setx 27
setxy 20 (-10)
setx 27
*/

pulisci

ritardo(10)

ripeti(38) {
  avanti(5)
  destra(10)
}

muoviVerso(55,posizione.y)
muoviVerso(27,posizione.y)
muoviVerso(20, -10)
muoviVerso(27,posizione.y)
