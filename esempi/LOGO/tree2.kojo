/*
; A different tree taken from
; http://utdallas.edu/~veerasam/logo/

make "angle1 15
make "angle2 30
make "factor1 0.9
make "factor2 0.8

to tree :level :size
  if :level > 0 [
    setpensize 1 + :level / 3
    if :level < 3 [
      setpencolor 10
    ]
    if :level >= 3 [
      setpencolor 0
    ]    fd :size
    lt :angle1
    tree :level - 1 :size * :factor1
    rt :angle1
    rt :angle2
    tree :level - 1 :size * :factor2
    lt :angle2
    pu bk :size pd
  ]
end

cs
window
pu setxy 0 -200 pd
tree 13 70 

Make changes to angle1, angle2, factor1 and factor2 to make your own wonderful tree!
*/

def albero(livello: Double, grandezza: Double,
           angolo1: Double, angolo2: Double,
           fattore1: Double, fattore2: Double) {
             
  seVero(livello > 0) {
    impostaSpessorePenna(livello / 2)
    se(livello >= 4) {
      colorePenna(marrone)
    } altrimenti {
      se(livello >= 2) {
        colorePenna(verde)
      } altrimenti {
        colorePenna(rosso)
      }
    }
    avanti(grandezza)
    sinistra(angolo1)
    albero(livello - 1, grandezza * fattore1, angolo1, angolo2, fattore1, fattore2)
    destra(angolo1 + angolo2)
    albero(livello - 1, grandezza * fattore2, angolo1, angolo2, fattore1, fattore2)
    sinistra(angolo2)
    alzaPenna()
    indietro(grandezza)
    abbassaPenna()
  }
}

pulisci()
ritardo(10)
invisibile()
alzaPenna()
indietro(200)
abbassaPenna()

albero(13, 60, 20, 20, 0.8, 0.8)

