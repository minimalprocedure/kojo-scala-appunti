/*
; Dahlia program by David Eisenstat
; from http://www.mathcats.com/gallery/15wordcontest.html

repeat 8 [rt 45 repeat 6 [repeat 90 [fd 2 rt 2] rt 90]]

; The 6 can be replaced with 1 to 7 for other flowers.
; For numbers greater than 7, the patterns repeat.
; This will work without modification in absolutely all Logo implementations.
*/

pulisci
ritardo(10)

ripeti(8) {
  destra(45)
  ripeti(6) {
    ripeti(90) {
      avanti(2)
      destra(2)
    }
    destra(90)
  }
}
