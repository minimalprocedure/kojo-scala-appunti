/*
; Polygon variation 3 by Paolo Passaro
; from http://www.mathcats.com/gallery/15wordcontest.html

repeat 8 [repeat 20 [lt 170 fd 20 rt 170 fd 20] rt 45]
*/

pulisci

ritardo(10)

ripeti(8) {
  ripeti(20){
    sinistra(170)
    avanti(20)
    destra(170)
    avanti(20)
  }
  destra(45)
}
