/*
; Starfish Program by Paolo Passaro and Julie Clune
; from http://www.mathcats.com/gallery/15wordcontest.html
; and http://www.mathcats.com/gallery/fiverosedetails.html

repeat 1800 [fd 10 rt repcount + .1 ]
*/

pulisci

ritardo(10)

ripetizione(1800) { i =>
  avanti(10)
  destra(i + 0.1)
}