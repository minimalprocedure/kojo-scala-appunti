/* -*- Scala -*- */

pulisci();
invisibile();
ritardo(20);

def sommaCifre1(numero: Int): Int = {
  val cifra = numero.toString
  var accumulatore = 0
  for (indice <- 0 to cifra.length - 1) {
    accumulatore = accumulatore + cifra(indice).asDigit
  }
  if (accumulatore > 9)
    accumulatore = sommaCifre(accumulatore)
  accumulatore
}

def sommaCifre2(numero: Int): Int = {
  var risultato = numero.toString.map(_.asDigit).foldLeft(0)(_ + _)
  if (risultato > 9)
    risultato = sommaCifre(risultato)
  risultato
}

def sommaCifre3(numero: Int): Int = {
  val risultato = numero.toString.map(_.asDigit).foldLeft(0)(_ + _)
  if (risultato > 9) sommaCifre(risultato)
  else risultato
}

def sommaCifre(numero: Int): Int = {
  numero % 9
}

def p9Moltiplicazione(fattore1: Int, fattore2: Int, prodotto: Int): List[Int] = {
  val valore1 = sommaCifre(fattore1)
  val valore2 = sommaCifre(fattore2)
  val valore3 = sommaCifre(prodotto)
  val valore4 = sommaCifre(valore1 * valore2)
  List(valore1, valore2, valore3, valore4)
}

def p9Divisione(dividendo: Int, divisore: Int, quoziente: Int, resto: Int): List[Int] = {
  val valore1 = sommaCifre(divisore)
  val valore2 = sommaCifre(quoziente)
  val valore3 = sommaCifre(dividendo)
  var valore4 = sommaCifre(valore1 * valore2) + resto
  valore4 = sommaCifre(valore4)
  List(valore1, valore2, valore3, valore4)
}

def p9DivisioneSenzaResto(dividendo: Int, divisore: Int, quoziente: Int): List[Int] = {
  p9Divisione(dividendo, divisore, quoziente, 0)
}

def p9Somma(addendo1: Int, addendo2: Int, somma: Int): List[Int] = {
  val valore1 = sommaCifre(addendo1)
  val valore2 = sommaCifre(addendo2)
  val valore3 = sommaCifre(somma)
  val valore4 = sommaCifre(valore1 + valore2)
  List(valore1, valore2, valore3, valore4)
}

def p9Sottrazione(minuendo: Int, sottraendo: Int, differenza: Int): List[Int] = {
  val valore1 = sommaCifre(minuendo)
  val valore2 = sommaCifre(sottraendo)
  val valore3 = sommaCifre(differenza)
  val valore4 = sommaCifre(valore1 - valore2)
  List(valore1, valore2, valore3, valore4)
}

def disegnaCroce() {
  indietro(100)
  casa()
  avanti(100)
  casa()
  destra()
  avanti(100)
  casa()
  sinistra()
  avanti(100)
  casa()
}

def scriviAlleCoordinate(testo: String, x: Int, y: Int) {
  saltaVerso(x, y)
  scrivi(testo)
}

def disegnaProva(valori: List[Int]) {
  scriviAlleCoordinate(valori(0).toString, -50, 50)
  scriviAlleCoordinate(valori(1).toString, 50, 50)
  scriviAlleCoordinate(valori(2).toString, -50, -50)
  scriviAlleCoordinate(valori(3).toString, 50, -50)
}

//val prova = p9Moltiplicazione(59714, 24339, 1453379046)
val prova = p9Divisione(59714, 24339, 2, 11036)
//val prova = p9Somma(59714, 24339, 84053)
//val prova = p9Sottrazione(59714, 24339, 35375)

/*
 ERRORE: la prova del nove può fallire nell' 11% dei casi.
 Se i due numeri sono diversi allora il risultato è errato
 Se i due numeri sono uguali allora il risultato può essere corretto

 Sottraendo al risultato 9 o un suo multiplo la prova risulterà soddisfatta
 mostrando un falso positivo.
 */
//val prova = p9Sottrazione(59714, 24339, 26294)
//val prova = p9Sottrazione(59714, 24339, 35375 - 9)
//val prova = p9Sottrazione(59714, 24339, 35375 - 18)
//val prova = p9Sottrazione(59714, 24339, 35375 - 27)
//val prova = p9Sottrazione(59714, 24339, 35375 + 81)

disegnaCroce()
disegnaProva(prova)
