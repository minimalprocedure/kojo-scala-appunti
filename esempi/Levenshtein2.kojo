/* -*- Scala -*- */
/* 
   Levenshtein - levenshtein distance calculator
   Nella teoria dell'informazione e nella teoria dei linguaggi, la distanza di Levenshtein, 
   o distanza di edit, è una misura per la differenza fra due stringhe. 
   Introdotta dallo scienziato russo Vladimir Levenshtein nel 1965, 
   serve a determinare quanto due stringhe siano simili. Viene applicata per esempio 
   per semplici algoritmi di controllo ortografico e per fare ricerca di 
   similarità tra immagini, suoni, testi, etc.

  La distanza di Levenshtein tra due stringhe A e B è il numero minimo di modifiche 
  elementari che consentono di trasformare la A nella B. 
  Per modifica elementare si intende

  la cancellazione di un carattere,
  la sostituzione di un carattere con un altro, o
  l'inserimento di un carattere.
  Per esempio, per trasformare "bar" in "biro" occorrono due modifiche:

  "bar" -> "bir" (sostituzione di 'a' con 'i')
  "bir" -> "biro" (inserimento di 'o')
  Non è possibile trasformare la prima parola nella seconda con meno di due modifiche, 
  quindi la distanza di Levenshtein fra "bar" e "biro" è 2.
*/

object Timing {
  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) / 1000 + "ms")
    result
  }
}

object Levenshtein {

  import scala.math._
  import scala.collection.mutable

  private var source = ""
  private var target = ""

  private def memoize[IN, OUT](f: IN => OUT): IN => OUT = new mutable.HashMap[IN, OUT]() {
    override def apply(key: IN) = getOrElseUpdate(key, f(key))
  }

  private def minimum(a: Int, b: Int, c: Int): Int = min(min(a, b), c)

  private def distance(i: Int, j: Int): Int = {
    (i, j) match {
      case (i, 0)                                   => i
      case (0, j)                                   => j
      case (i, j) if source(i - 1) == target(j - 1) => distance(i - 1, j - 1)
      case (i, j) => 1 + minimum(
        distance(i - 1, j),
        distance(i, j - 1),
        distance(i - 1, j - 1)
      )
    }
  }

  private val distance_memo: ((Int, Int)) => Int = memoize {
    case (i, 0)                                   => i
    case (0, j)                                   => j
    case (i, j) if source(i - 1) == target(j - 1) => distance_memo(i - 1, j - 1)
    case (i, j) => 1 + minimum(
      distance_memo(i - 1, j),
      distance_memo(i, j - 1),
      distance_memo(i - 1, j - 1)
    )
  }

  def calc(source: String, target: String): Int = {
    this.source = source
    this.target = target
    distance(source.length, target.length)
  }

  def calc_memo(source: String, target: String): Int = {
    this.source = source
    this.target = target
    distance_memo(source.length, target.length)
  }
}

val s = "levenshtein"
val t = "levenshtain"

println("START: memoized")
val res2 = Timing.time { Levenshtein.calc_memo(s, t) }
println(res2)

println("START: normal")
val res0 = Timing.time { Levenshtein.calc(s, t) }
println(res0)
