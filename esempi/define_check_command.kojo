clear

def check(test: Boolean) (then: => Unit) (that: => Unit) {
  if(test) then else that
}

val n = 4

check(n > 3) {
  println("vero")
} {
  println("falso")
}
