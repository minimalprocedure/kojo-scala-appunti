/* -*- Scala -*- */

/*
 FizzBuzz
 */

pulisci()
alzaPenna()

def divisibile(num: Int, div: Int): Boolean = {
  (num % div) == 0
}

def tartaScrivi(messaggio: String) {
  avanti(20)
  abbassaPenna()
  scrivi(messaggio)
  alzaPenna()
}

def fizzBuzz0(da: Int, a: Int) {
  val sequenzaDiNumeri = da to a
  ripetiPerOgniElementoDi(sequenzaDiNumeri) { numero =>
    val messaggio =
      if (divisibile(numero, 3) && divisibile(numero, 5))
        "FizzBuzz"
      else if (divisibile(numero, 3))
        "Fizz"
      else if (divisibile(numero, 5))
        "Buzz"
      else
        numero.toString
    tartaScrivi(messaggio)
  }
}

def fizzBuzz(da: Int, a: Int) {
  for (numero <- da to a) {
    val messaggio =
      if (divisibile(numero, 3) && divisibile(numero, 5))
        "FizzBuzz"
      else if (divisibile(numero, 3))
        "Fizz"
      else if (divisibile(numero, 5))
        "Buzz"
      else
        numero.toString
    tartaScrivi(messaggio)
  }
}



fizzBuzz(1, 100)
