/*
  Android Sensors

  Simple example for reading smartphone sensors values.
  You must install in you smartphone Android-Sensors-REST from github repository:
  https://github.com/minimalprocedure/Android-Sensors-REST
  
*/

/* main program */
import scalaj.http._
import scala.util.parsing.json.JSON

val IPAddress = "127.0.0.1"
val port = 9999

type MapData = Map[String, _]
type ListOfMapData = List[Map[String, _]]

def resumeSensor(ip: String, port: Int, sensorName: String) =
  Http(s"http://$ip:$port/sensor/$sensorName/resume").asString

def pauseSensor(ip: String, port: Int, sensorName: String) =
  Http(s"http://$ip:$port/sensor/$sensorName/pause").asString

def getSensor(ip: String, port: Int, sensorName: String): MapData = {
  val response = Http(s"http://$ip:$port/sensor/$sensorName").asString
  val json = JSON.parseFull(response.body)
  println(response.body)
  json match {
    case None       => Map()
    case Some(json) => json.asInstanceOf[MapData]
  }
}

clearOutput()
resumeSensor(IPAddress, port, "accelerometer")
pause(0.5)
getSensor(IPAddress, port, "accelerometer")
pause(0.5)
pauseSensor(IPAddress, port, "accelerometer")

//pause(0.5)

resumeSensor(IPAddress, port, "magnetic_field")
pause(0.5)
getSensor(IPAddress, port, "magnetic_field")
pause(0.5)
pauseSensor(IPAddress, port, "magnetic_field")

