pulisci()
//showGrid()
invisibile()

val tarta = nuovaTartaruga(0, 0)
loadImages("./bat-up-a.png", "./bat-up-b.png")

var svolazzoVelocità = 0.2
var velocità = 1

def svolazza() {
  tarta.fai { t =>
    ripetiFinché(true) {
      pause(svolazzoVelocità)
      t.prossimoCostume()
    }
  }
}
svolazza()

def loadImages(fnameA: String, fnameB: String) {
  import net.kogics.kojo.util.Utils.loadImage
  val a = loadImage(fnameA)
  val b = loadImage(fnameB)
  tarta.englishTurtle.setCostumeImages(a, b)
}

var tastoAttivo = Kc.VK_UP

def cambiaCostumi() {

  if (isKeyPressed(Kc.VK_LEFT)) {
    if (tastoAttivo != Kc.VK_LEFT) {
      scriviLinea("Kc.VK_LEFT")
      loadImages("./bat-left-a.png", "./bat-left-b.png")
      tastoAttivo = Kc.VK_LEFT

    }
  }
  else if (isKeyPressed(Kc.VK_RIGHT)) {
    if (tastoAttivo != Kc.VK_RIGHT) {
      scriviLinea("Kc.VK_RIGHT")
      loadImages("./bat-right-a.png", "./bat-right-b.png")
      tastoAttivo = Kc.VK_RIGHT
      svolazzoVelocità = 0.1
    }
  }
  else if (isKeyPressed(Kc.VK_UP)) {
    if (tastoAttivo != Kc.VK_UP) {
      scriviLinea("Kc.VK_UP")
      loadImages("./bat-up-a.png", "./bat-up-b.png")
      tastoAttivo = Kc.VK_UP
      svolazzoVelocità = 0.1
    }
  }
  else if (isKeyPressed(Kc.VK_DOWN)) {
    if (tastoAttivo != Kc.VK_DOWN) {
      scriviLinea("Kc.VK_DOWN")
      loadImages("./bat-down-a.png", "./bat-down-b.png")
      tastoAttivo = Kc.VK_DOWN
      svolazzoVelocità = 0.3
    }
  }
}

def muovi() {

  val x = tarta.posizione.x;
  val y = tarta.posizione.y;

  svolazzoVelocità = 0.2
  velocità = if (isKeyPressed(Kc.VK_SHIFT)) 4 else 1

  if (isKeyPressed(Kc.VK_LEFT) && isKeyPressed(Kc.VK_UP)) {
    svolazzoVelocità = 0.1
    tarta.saltaVerso(x - velocità, y + velocità)
  }
  else if (isKeyPressed(Kc.VK_LEFT) && isKeyPressed(Kc.VK_DOWN)) {
    svolazzoVelocità = 0.4
    tarta.saltaVerso(x - velocità, y - velocità)
  }
  else if (isKeyPressed(Kc.VK_RIGHT) && isKeyPressed(Kc.VK_UP)) {
    svolazzoVelocità = 0.1
    tarta.saltaVerso(x + velocità, y + velocità)
  }
  else if (isKeyPressed(Kc.VK_RIGHT) && isKeyPressed(Kc.VK_DOWN)) {
    svolazzoVelocità = 0.4
    tarta.saltaVerso(x + velocità, y - velocità)
  }
  else if (isKeyPressed(Kc.VK_LEFT)) {
    svolazzoVelocità = 0.2
    tarta.saltaVerso(x - velocità, y)
  }
  else if (isKeyPressed(Kc.VK_RIGHT)) {
    svolazzoVelocità = 0.2
    tarta.saltaVerso(x + velocità, y)
  }
  else if (isKeyPressed(Kc.VK_UP)) {
    svolazzoVelocità = 0.1
    tarta.saltaVerso(x, y + velocità)
  }
  else if (isKeyPressed(Kc.VK_DOWN)) {
    svolazzoVelocità = 0.4
    tarta.saltaVerso(x, y - velocità)
  }
}

animate {

  //ferma il loop infinito di animazione
  if (isKeyPressed(Kc.VK_ESCAPE)) {
    stopAnimation()
  }

  cambiaCostumi()
  muovi()

}
