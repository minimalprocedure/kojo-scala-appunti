/* -*- Scala -*- */

pulisci()

ritardo(20)
invisibile()

def spirale(giri: Int, spazio: Double, verso: Direzione) {
  val distanza = 360 / spazio
  val gradi = giri * 360
  val incr = 4
  val rotazione = 2
  var delta = 1
  ripeti(gradi) {
    avanti(delta / distanza)
    verso match {
      case Destra   => destra(rotazione)
      case Sinistra => sinistra(rotazione)
    }
    delta = delta + incr
  }
  avanti((delta / distanza) / rotazione)
}

spirale(2, 1, Sinistra)
spirale(2, 1, Destra)
