/* Look-and-say - Conway sequence printer
  MMG - zairik@gmail.com - Public Domain
*/

clearOutput()

def las1(list: List[Int]): List[Int] = {
  list match {
    case head :: Nil => List(head, head)
    case head :: tail => {
      val quantity = list.takeWhile(_ == head).size
      List(quantity, head) ::: las1(list.drop(quantity))
    }
    case _ => list
  }
}

def las2(list: List[Int]): List[Int] = {
  if (list.isEmpty) list
  else if (list.size == 1) List(list(0), list(0))
  else {
    val quantity = list.takeWhile(_ == list.head).size
    List(quantity, list.head) ::: las2(list.drop(quantity))
  }
}

def lasT(arg: (List[Int], List[Int])): List[Int] = {
  arg match {
    case (Nil, t)                         => t.reverse
    case (h :: hs, Nil)                   => lasT((hs, h :: 1 :: Nil))
    case (h :: hs, a :: b :: t) if h == a => lasT((hs, a :: 1 + b :: t))
    case (h :: hs, t)                     => lasT((hs, h :: 1 :: t))
  }
}

def genLAS(base: Int, limit: Int, lasFunc: (List[Int]) => List[Int]): String = {
  val seq = Range(1, limit).foldLeft(
    List[List[Int]](base :: Nil))((memo, v) => memo :+ lasFunc(memo.last))
  seq.map(_.mkString).mkString("\n")
}

def genLAS2(base: Int, limit: Int, lasFunc: (Seq[Int]) => Seq[Int]): String = {
  val seq = Range(1, limit).foldLeft(
    Seq[Seq[Int]](base :: Nil))((memo, v) => memo :+ lasFunc(memo.last))
  seq.map(_.mkString).mkString("\n")
}

def genLASt(base: Int, limit: Int) = {
  val seq = Range(1, limit).foldLeft(
    List[List[Int]](base :: Nil))((memo, v) => memo :+ lasT((memo.last, List())))
  seq.map(_.mkString).mkString("\n")
}

val base = 1
val limit = 8
val warm = 1000

/* warmimg allocation values */
var t0 = System.nanoTime()
var t1 = System.nanoTime()

println("-----------------------------------------------------------------")
println("las 1 ")
println("-----------------------------------------------------------------")
Range(1, warm).foreach { x => genLAS(base, limit, las1) }
t0 = System.nanoTime()
println(genLAS(base, limit, las1))
t1 = System.nanoTime()
println("")
println("Execution time: " + (t1 - t0) / 1000.0 + "ms")
println("")
println("-----------------------------------------------------------------")
println("las 2 ")
println("-----------------------------------------------------------------")
Range(1, warm).foreach { x => genLAS(base, limit, las2) }
t0 = System.nanoTime()
println(genLAS(base, limit, las2))
t1 = System.nanoTime()
println("")
println("Execution time: " + (t1 - t0) / 1000.0 + "ms")
println("")
println("-----------------------------------------------------------------")
println("las T ")
println("-----------------------------------------------------------------")
Range(1, warm).foreach { x => genLASt(base, limit) }
t0 = System.nanoTime()
println(genLASt(base, limit))
t1 = System.nanoTime()
println("")
println("Execution time: " + (t1 - t0) / 1000.0 + "ms")

