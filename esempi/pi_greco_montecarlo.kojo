/*
  π calculation with MonteCarlo method
*/

def π(i: Int): Double = {
  val n = (0 until i).foldLeft(0.0) ((acc, n) => {
    val x = Math.random
    val y = Math.random
    acc + (if (x*x + y*y <= 1.0) 1.0 else 0.0)
  })
  4*n/i
}

println(π(100))
println(π(1000))
println(π(10000))
println(π(100000))
println(π(1000000))
println(π(10000000))
//println(π(100000000))
