import javafx.embed.swing.JFXPanel
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.scene.control.Button
import javafx.event.ActionEvent
import javafx.scene.input.MouseEvent
import javafx.event.EventHandler
import javax.swing.JFrame

// Implicito per usare una closure come handler di evento: scala < scala 2.12
import scala.language.implicitConversions
object FXEvent2HandlerImplicits {
  implicit def mouseEvent2EventHandler(event: MouseEvent => Unit) = new EventHandler[MouseEvent] {
    override def handle(hEvent: MouseEvent): Unit = event(hEvent)
  }
}

class FXWindow(title: String, panel: JFXPanel) {

  val frame = new JFrame(title)
  frame.add(panel)
  frame.setSize(panel.getSize)
  frame.setLocation(100, 100)
  frame.setVisible(true);

}

class AppContent extends JFXPanel {

  import FXEvent2HandlerImplicits._

  val root = new Group()
  val scene = new Scene(root, Color.ALICEBLUE)

  val text = new Text()
  text.setX(40)
  text.setY(100)
  text.setFont(new Font(25))
  text.setText("JavaFx")
  text.setOnMouseEntered((ev: MouseEvent) => text.setText("JavaFx over!"))
  text.setOnMouseExited((ev: MouseEvent) => text.setText("JavaFx"))

  val button = new Button("click")
  button.setLayoutX(40)
  button.setLayoutY(140)
  button.setOnMouseClicked((ev: MouseEvent) => text.setText("JavaFx Click!"))

  root.getChildren.add(text)
  root.getChildren.add(button)

  this.setScene(scene)
  this.setSize(300, 300)

}

new FXWindow("vanilla JavaFx Text", new AppContent())
