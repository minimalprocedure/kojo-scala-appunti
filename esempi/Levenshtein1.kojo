/* -*- Scala -*- */
/* Levenshtein distance 1
  MMG - zairik@gmail.com - Public Domain
*/

import scala.math.min

val source = "levenshtein"
val target = "levenshtain"

def minimum(a: Int, b: Int, c: Int): Int = {
  val m = min(a, b)
  min(m, c)
}

def cost(i: Int, j: Int): Int =
  if (source(i - 1) == target(j - 1)) 0 else 1

def dist_match(i: Int, j: Int): Int = {
  (i, j) match {
    case (i, 0) => i
    case (0, j) => j
    case (i, j) => minimum(
      dist_match(i - 1, j) + 1,
      dist_match(i, j - 1) + 1,
      dist_match(i - 1, j - 1) + cost(i, j)
    )
  }
}

def lev_match(source: String, target: String): Int = {
  def dist(i: Int, j: Int): Int = {
    (i, j) match {
      case (i, 0) => i
      case (0, j) => j
      case (i, j) => minimum(
        dist(i - 1, j) + 1,
        dist(i, j - 1) + 1,
        dist(i - 1, j - 1) + cost(i, j)
      )
    }
  }
  dist(source.length, target.length)
}

def lev_if(source: String, target: String): Int = {
  def dist(i: Int, j: Int): Int = {
    if (i > 0 && j == 0)
      i
    else if (i == 0 && j > 0)
      j
    else if (i > 0 && j > 0) {
      minimum(
        dist_if(i - 1, j) + 1,
        dist_if(i, j - 1) + 1,
        dist_if(i - 1, j - 1) + cost(i, j)
      )
    }
    else 0
  }
  dist(source.length, target.length)
}

def dist_if(i: Int, j: Int): Int = {
  if (i > 0 && j == 0)
    i
  else if (i == 0 && j > 0)
    j
  else if (i > 0 && j > 0) {
    minimum(
      dist_if(i - 1, j) + 1,
      dist_if(i, j - 1) + 1,
      dist_if(i - 1, j - 1) + cost(i, j)
    )
  }
  else 0
}

def lev_matrix(source: String, target: String): Int = {
  val n = source.length
  val m = target.length
  val d = Array.ofDim[Int](n + 1, m + 1)
  for (i <- 1 to n) d(i)(0) = i
  for (j <- 1 to m) d(0)(j) = j

  for {
    i <- 1 to n
    j <- 1 to m
  } {
    val cost = if (source(i - 1) == target(j - 1)) 0 else 1
    d(i)(j) = minimum(
      d(i - 1)(j) + 1,
      d(i)(j - 1) + 1,
      d(i - 1)(j - 1) + cost)
    //println(s"(${i})(${j}): ${d(i)(j)} - ${source(i - 1)}, ${target(j - 1)}")
  }
  d(n)(m)
}

def time[R](block: => R): R = {
  val t0 = System.nanoTime()
  val result = block
  val t1 = System.nanoTime()
  println("Elapsed time: " + (t1 - t0) / 1000 + "ms")
  result
}

clearOutput

var res = 0

println("dist_match:")
res = time { dist_match(source.length, target.length) }
println("result: " + res)
println("-------------------------")

println("dist_if:")
res = time { dist_if(source.length, target.length) }
println("result: " + res)
println("-------------------------")

println("lev_match:")
res = time { lev_match(source, target) }
println("result: " + res)
println("-------------------------")

println("lev_if:")
res = time { lev_if(source, target) }
println("result: " + res)
println("-------------------------")

println("lev_matrix:")
res = time { lev_matrix(source, target) }
println("result: " + res)

