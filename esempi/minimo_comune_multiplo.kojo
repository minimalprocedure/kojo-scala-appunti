/*
  Calcolo del Massimo Comun Divisore e Minimo Comune Multiplo con l'Algoritmo di Euclide

Da Wikipedia, l'enciclopedia libera.
https://it.wikipedia.org/wiki/Algoritmo_di_Euclide

L'algoritmo di Euclide è un algoritmo per trovare il massimo comune divisore 
(indicato di seguito con MCD) tra due numeri interi. È uno degli algoritmi più 
antichi conosciuti, essendo presente negli Elementi di Euclide[1] intorno al 300 a.C.; 
tuttavia, probabilmente l'algoritmo non è stato scoperto da Euclide, ma potrebbe 
essere stato conosciuto anche 200 anni prima. Certamente era conosciuto da 
Eudosso di Cnido intorno al 375 a.C.; Aristotele (intorno al 330 a.C.) ne ha 
fatto cenno ne I topici, 158b, 29-35. L'algoritmo non richiede la fattorizzazione dei due interi.

Dati due numeri naturali a e b, si controlla se b è zero (questa prima fase rientra ovviamente 
nell'ambito di un uso moderno dell'algoritmo ed era ignorata da Euclide e dai suoi predecessori, 
che non conoscevano lo zero). Se lo è, a è il MCD. Se non lo è, si divide a / b e si assegna ad r 
il resto della divisione (operazione indicata con "a modulo b" più sotto). Se r = 0 allora 
si può terminare affermando che b è il MCD cercato, altrimenti occorre assegnare a = b e b = r e 
si ripete nuovamente la divisione. L'algoritmo può essere anche espresso in modo naturale 
utilizzando la ricorsione in coda.

*/

val number1 = 210
val number2 = 45

def maxComDiv(dividend: Int, divisor:Int) : Int = {
  val (oldRemainder, newRemainder) = (divisor, dividend % divisor)
  newRemainder match {
    case 0 => oldRemainder
    case _ => maxComDiv(oldRemainder, newRemainder)
  } 
}

def minComDiv(num1: Int, num2: Int): Int = {
  (num1 * num2) / maxComDiv(num1, num2)
}

val res1 = maxComDiv(number1, number2)
println(res1)

val res2 = minComDiv(number1, number2)
println(res2)