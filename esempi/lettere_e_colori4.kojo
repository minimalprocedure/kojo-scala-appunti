/* -*- Scala -*- */

/*
 Cifratura colorata 4
 */

/*
 La funzione prende il colore di un pixel alle coordinate date.
 Leggendolo dall'area di disegno direttamente
 */
def prendiColoreAlPixel(x: Int, y: Int): Color = {
  val cameraBounds = stage.canvas.getCamera.getBounds
  val centerX = cameraBounds.getCenterX.toInt
  val centerY = cameraBounds.getCenterY.toInt
  val canvasX = (centerX.toInt + x)
  val canvasY = (centerY.toInt - y)
  val pickpath = stage.canvas.getCamera.pick(canvasX, canvasY, 1)
  val paint = pickpath.getPickedNode.getPaint
  if (paint != null)
    paint.asInstanceOf[Color]
  else
    Color(255, 255, 255, 255)
}

/*
 Funzione per disegnare poligoni regolari
 */
def poligono(lati: Int, lato: Int, colore1: Color, colore2: Color) {
  val angolo = 360 / lati.toDouble
  colorePenna(colore1)
  coloreRiempimento(colore2)
  seVero(lati % 2 >= 0) {
    sinistra(90 - angolo);
  }
  ripeti(lati) {
    avanti(lato)
    destra(angolo)
  }
}

/*
 Il programma genera una immagine partendo da un testo e poi la rilegge riestraendo le lettere.
 */
pulisci()
switchToDefault2Perspective()
ritardo(10)
invisibile()

val X = -300
val Y = 150

def scriviAllaPosizione(testo: String, x: Double, y: Double, colore: Color) {
  val t = nuovaTartaruga(x, y)
  t.invisibile()
  t.colorePenna(colore)
  t.scrivi(testo)
}

/*
 Funzione per generare un quadrato con il colore di riempimento in base alla lettera. Il codice della lettera
 è immagazzinato nel canale rosso del colore, i rimanenti verde e blu sono casuali.
 */
def letteraInQuadrato(lettera: Char, lato: Int = 50, marca: Boolean = false) {
  val coloreVerde = numeroCasuale(255)
  val coloreBlu = numeroCasuale(255)
  val coloreRosso = lettera.toInt
  val colore = new Color(coloreRosso, coloreVerde, coloreBlu, 255)
  poligono(4, lato, colore, colore)
  if (marca)
    scriviAllaPosizione(lettera.toString, posizione.x + (lato / 2), posizione.y + (lato / 2), nero)
}

/*
 La funzione codifica un testo in una sequenza di quadrati colorati.
 */
def codifica(testo: String, inizioX: Int, inizioY: Int, colonne: Int = 10, larghezzaColonna: Int = 50) {
  saltaVerso(inizioX, inizioY)
  var colonna = 1
  var riga = 0
  ripetiPerOgniElementoDi(testo) { lettera =>
    letteraInQuadrato(lettera, larghezzaColonna)
    colonna += 1
    if (colonna > colonne) {
      colonna = 1
      riga += 1
      saltaVerso(inizioX, inizioY - larghezzaColonna * riga)
    }
    else {
      saltaVerso(posizione.x + larghezzaColonna, posizione.y)
    }
  }
  poligono(4, larghezzaColonna, nero, nero)
}

/*
 La funzione rilegge l'immagine generata estraendo il testo.
 La larghezza della colonna deve essere variata per adattarla all'immagine, la password insomma.
 */
def rileggi(startX: Int, startY: Int, larghezzaColonna: Int = 50): String = {
  val centroDellaColonna = larghezzaColonna / 2
  var x = startX + centroDellaColonna
  var y = startY + centroDellaColonna
  var colore = bianco
  var testo = ""
  ripetiFinché(colore != nero) {
    colore = prendiColoreAlPixel(x, y)
    //nuovaTartaruga(x,y)
    if (colore != bianco) {
      testo += colore.getRed.toChar
      x += larghezzaColonna
    }
    else {
      x = startX + centroDellaColonna
      y -= larghezzaColonna
    }
  }
  testo
}

/*
 Invocazione delle funzioni
 */

val testo = "Oggi esco a passeggio con la mia amica Gigia, ma poi andiamo al cinema insieme."
scriviAllaPosizione("testo:  " + testo, -400, -250, nero)
codifica(testo, X, Y)

val colore = prendiColoreAlPixel(175, -175)
val testoRiletto = rileggi(X, Y)
scriviAllaPosizione("riletto: " + testoRiletto, -400, -270, rosso)
