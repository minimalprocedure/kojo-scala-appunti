/* -*- Scala -*- */
//Cifrario di Vigenère

val testo = "Caro amico ci vogliamo incontrare stasera per giocare?"
val password = "pippo"

def calcolaVermeFunzioneLunga(password: String, lunghezzaTesto: Int): String = {
  val ripetizioni = lunghezzaTesto / password.length
  val resto = lunghezzaTesto % password.length
  val tappo = se(resto > 0) {
    password.slice(0, resto)
  } altrimenti {
    ""
  }
  (password * ripetizioni) + tappo
}

def calcolaVerme(password: String, lunghezzaTesto: Int): String = {
  val ripetizioni = (lunghezzaTesto.toDouble / password.length).ceil.toInt
  val verme = password * ripetizioni
  verme.slice(0, lunghezzaTesto)
}

def cifra(testo: String, password: String): String = {
  val verme = calcolaVerme(password, testo.length)
  var testoCifrato = ""
  ripetizione(testo.length) { indice =>
    val posizione = indice - 1
    val numero = testo(posizione).toInt
    val numeroVerme = verme(posizione).toInt
    val numeroLetteraCifrata = numero + numeroVerme
    val letteraCifrata = numeroLetteraCifrata.toChar
    testoCifrato = testoCifrato + letteraCifrata
  }
  testoCifrato
}

def decifra(testoCifrato: String, password: String): String = {
  val verme = calcolaVerme(password, testo.length)
  var testoDecifrato = ""
  ripetizione(testo.length) { indice =>
    val posizione = indice - 1
    val numero = testoCifrato(posizione).toInt
    val numeroVerme = verme(posizione).toInt
    val numeroletteraDecifrata = numero - numeroVerme
    val letteraDecifrata = numeroletteraDecifrata.toChar
    testoDecifrato = testoDecifrato + letteraDecifrata
  }
  testoDecifrato
}

val testoCifrato = cifra(testo, password)
val testoDecifrato = decifra(testoCifrato, password)


pulisci()
invisibile()
colorePenna(nero)
saltaVerso(-400, 0)
scrivi("testo: " + testo)
salta(-25)
scrivi("password: " + password)
salta(-25)
scrivi("testo cifrato: " + testoCifrato)
salta(-25)
scrivi("testo decifrato: " + testoDecifrato)
