import javafx.embed.swing.JFXPanel
import javafx.scene.Scene
import javafx.scene.control._
import javafx.stage.Stage

// Force initialization JavaFX by creating JFXPanel() object
// (we will not use it for anything else)
new JFXPanel()

// Run on JavaFX Application thread
scalafx.application.Platform.runLater {
  val stage = new Stage()
  val message = new javafx.scene.control.Label("Hello?")
  stage.setScene(new Scene(message))
  stage.show()
  stage.onCloseRequestProperty
}