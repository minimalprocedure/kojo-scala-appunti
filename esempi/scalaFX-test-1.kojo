pulisci()

import scalafx.scene._
import scalafx.scene.paint._
import scalafx.embed.swing.SFXPanel
import scalafx.application.Platform

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.paint.Color._
import scalafx.scene.shape.Rectangle



def initScene(w: Int, h: Int, background: Paint): Scene = {
  new Scene(w, h) {
    fill = background
    content = new Rectangle {
      x = 25
      y = 40
      width = 100
      height = 100
      fill <== when(hover) choose Green otherwise Red
    }
  }
}

def initPanel(scene: Scene): SFXPanel = {
  val panel = new SFXPanel()
  panel.setScene(scene)
  panel
}

val scene = initScene(400, 300, scalafx.scene.paint.Color.DimGrey)
val panel = initPanel(scene)

val pic = PicShape.widget(panel)

draw(trans(canvasBounds.x, canvasBounds.y) -> pic)
