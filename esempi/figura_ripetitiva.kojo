/* -*- Scala -*- */

pulisci()

ritardo(20)

def poligono(lato: Double, lati: Int): Double = {
  val angolo = 360 / lati.toDouble;
  seVero(lati % 2 >= 0) {
    sinistra(90 - angolo);
  }
  colorePenna(nero);
  ripeti(lati) {
    avanti(lato);
    destra(angolo);
  }
  perimetro;
}

def figuraRipetitiva(lato: Int, lati: Int, decremento: Int) {
  var latoVariabile = lato
  ripeti(latoVariabile / decremento) {
    poligono(latoVariabile, lati)
    latoVariabile = latoVariabile - decremento
  }
}

figuraRipetitiva(200, 5, 12)

