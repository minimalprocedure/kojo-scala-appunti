/* -*- Scala -*- */

import net.kogics.kojo.util.Utils.loadImage
import net.kogics.kojo.picture.ImagePic
import net.kogics.kojo.core.Point

pulisci()
invisibile()
switchToDefault2Perspective()
drawStage(Color(255, 255, 255, 0))
//showGrid()

val COORDINATE_STAGE = stage.bounds
val COORDINATE_PERSONAGGIO = Point(COORDINATE_STAGE.x + 100, 0.0)
val COORDINATE_AMBIENTE = Point(3000 / 2 -  COORDINATE_STAGE.width / 2, 0.0)

/* 
Direzioni 
*/
val STOP = 0
val SINISTRA = 100
val DESTRA = 200
val ALTO = 300
val BASSO = 400
val SINISTRA_ALTO = 500
val SINISTRA_BASSO = 600
val DESTRA_ALTO = 700
val DESTRA_BASSO = 800
var DIREZIONE = ALTO

def direzionePersonaggio(): Int = {
  DIREZIONE = if (isKeyPressed(Kc.VK_LEFT) && isKeyPressed(Kc.VK_UP))
    SINISTRA_ALTO
  else if (isKeyPressed(Kc.VK_LEFT) && isKeyPressed(Kc.VK_DOWN))
    SINISTRA_BASSO
  else if (isKeyPressed(Kc.VK_RIGHT) && isKeyPressed(Kc.VK_UP))
    DESTRA_ALTO
  else if (isKeyPressed(Kc.VK_RIGHT) && isKeyPressed(Kc.VK_DOWN))
    DESTRA_BASSO
  else if (isKeyPressed(Kc.VK_LEFT))
    SINISTRA
  else if (isKeyPressed(Kc.VK_RIGHT))
    DESTRA
  else if (isKeyPressed(Kc.VK_UP))
    ALTO
  else if (isKeyPressed(Kc.VK_DOWN))
    BASSO
  else
    STOP
  DIREZIONE
}

/*
  Immagini del personaggio per le varie direzioni
*/
val COSTUMI_SU = List[Image](
  loadImage("./immagini/up-a.png"),
  loadImage("./immagini/up-b.png")
)

val COSTUMI_SINISTRA = List[Image](
  loadImage("./immagini/left-a.png"),
  loadImage("./immagini/left-b.png")
)

val COSTUMI_DESTRA = List[Image](
  loadImage("./immagini/right-a.png"),
  loadImage("./immagini/right-b.png")
)

val COSTUMI_GIU = List[Image](
  loadImage("./immagini/down-a.png"),
  loadImage("./immagini/down-b.png")
)

var COSTUMI = List[Image]()

/* 
  Personaggio 
*/
def nuovoPersonaggio(): Tartaruga = {
  val personaggio = nuovaTartaruga(COORDINATE_PERSONAGGIO.x, COORDINATE_PERSONAGGIO.y)
  rettangoloDiCollisionePersonaggio = PicShape.image(loadImage("./immagini/collision-bordo.png"))
  rettangoloDiCollisionePersonaggio.setPosition(personaggio.posizione.x - 50, personaggio.posizione.y - 71)
  draw(rettangoloDiCollisionePersonaggio)
  animaPersonaggio(personaggio)
  personaggio
}

def animaPersonaggio(personaggio: Tartaruga) {
  personaggio.fai { t =>
    ripetiFinché(true) {
      pause(velocitàAnimazionPersonaggio)
      t.prossimoCostume()
    }
  }
}

def vestiPersonaggio(personaggio: Tartaruga, immagini: List[Image]) {
  if (COSTUMI != immagini) {
    COSTUMI = immagini
    personaggio.indossaImmagini(immagini.toVector)
  }
}

def adattaCostumiPersonaggio(personaggio: Tartaruga) {
  direzionePersonaggio() match {
    case SINISTRA       => vestiPersonaggio(personaggio, COSTUMI_SINISTRA)
    case DESTRA         => vestiPersonaggio(personaggio, COSTUMI_DESTRA)
    case ALTO           => vestiPersonaggio(personaggio, COSTUMI_SU)
    case BASSO          => vestiPersonaggio(personaggio, COSTUMI_GIU)
    case SINISTRA_ALTO  => vestiPersonaggio(personaggio, COSTUMI_SINISTRA)
    case SINISTRA_BASSO => vestiPersonaggio(personaggio, COSTUMI_SINISTRA)
    case DESTRA_ALTO    => vestiPersonaggio(personaggio, COSTUMI_DESTRA)
    case DESTRA_BASSO   => vestiPersonaggio(personaggio, COSTUMI_DESTRA)
    case _              => vestiPersonaggio(personaggio, COSTUMI_SU)
  }
}

/* 
  Velocità 
*/
val VELOCITA_LENTA = 0.4
val VELOCITA_NORMALE = 0.2
val VELOCITA_VELOCE = 0.1
val ACCELERAZIONE_LENTA = 1
val ACCELERAZIONE_VELOCE = 4

def velocitàAnimazionPersonaggio(): Double = {
  direzionePersonaggio() match {
    case SINISTRA       => VELOCITA_NORMALE
    case DESTRA         => VELOCITA_NORMALE
    case ALTO           => VELOCITA_VELOCE
    case BASSO          => VELOCITA_LENTA
    case SINISTRA_ALTO  => VELOCITA_VELOCE
    case SINISTRA_BASSO => VELOCITA_LENTA
    case DESTRA_ALTO    => VELOCITA_VELOCE
    case DESTRA_BASSO   => VELOCITA_LENTA
    case _              => VELOCITA_NORMALE
  }
}

def incrementoVelocitàPersonaggio(): Double = {
  if (isKeyPressed(Kc.VK_SHIFT))
    ACCELERAZIONE_VELOCE
  else
    ACCELERAZIONE_LENTA
}

def muoviPersonaggio(personaggio: Tartaruga) {
  val x = personaggio.posizione.x;
  val y = personaggio.posizione.y;
  val posizione = direzionePersonaggio() match {
    case SINISTRA       => (x - incrementoVelocitàPersonaggio, y)
    case DESTRA         => (x + incrementoVelocitàPersonaggio, y)
    case ALTO           => (x, y + incrementoVelocitàPersonaggio)
    case BASSO          => (x, y - incrementoVelocitàPersonaggio)
    case SINISTRA_ALTO  => (x - incrementoVelocitàPersonaggio, y + incrementoVelocitàPersonaggio)
    case SINISTRA_BASSO => (x - incrementoVelocitàPersonaggio, y - incrementoVelocitàPersonaggio)
    case DESTRA_ALTO    => (x + incrementoVelocitàPersonaggio, y + incrementoVelocitàPersonaggio)
    case DESTRA_BASSO   => (x + incrementoVelocitàPersonaggio, y - incrementoVelocitàPersonaggio)
    case _              => (x, y)
  }
  rettangoloDiCollisionePersonaggio.setPosition(posizione._1 - 50, posizione._2 - 71)
  personaggio.saltaVerso(posizione._1, posizione._2)
}

/* 
  Collisioni 
*/

var rettangoloDiCollisionePersonaggio: ImagePic = _

def controllaCollisioniPersonaggio() {

  if (rettangoloDiCollisionePersonaggio.collidesWith(stageRight)) {
    //println("collide right")
    val x = AMBIENTE.posizione.x - COORDINATE_STAGE.width
    AMBIENTE.saltaVerso(x, AMBIENTE.posizione.y)
    PERSONAGGIO.saltaVerso(COORDINATE_STAGE.x + 100, PERSONAGGIO.posizione.y)
  }

  if (rettangoloDiCollisionePersonaggio.collidesWith(stageLeft)) {
    //println("collide left")
    val x1 = AMBIENTE.posizione.x + COORDINATE_STAGE.width
    val x2 = COORDINATE_STAGE.x + COORDINATE_STAGE.width - 100
    AMBIENTE.saltaVerso(x1, AMBIENTE.posizione.y)
    PERSONAGGIO.saltaVerso(x2 - 100, PERSONAGGIO.posizione.y)
  }
}

/* 
  Ambiente 
*/

def caricaAmbiente(f: String): Tartaruga = {
  val immagine = loadImage(f)
  val ambiente = nuovaTartaruga(COORDINATE_AMBIENTE.x, 0)  
  ambiente.indossaImmagine(immagine)
  println(immagine.getWidth(null))
  ambiente
}

def fermaGiocoSeEsc() {
  //ferma il loop infinito di animazione
  if (isKeyPressed(Kc.VK_ESCAPE)) {
    stopAnimation()
  }
}

val AMBIENTE = caricaAmbiente("./immagini/sfondo2.png")
val PERSONAGGIO = nuovoPersonaggio()

animate {
  fermaGiocoSeEsc()
  controllaCollisioniPersonaggio()
  adattaCostumiPersonaggio(PERSONAGGIO)
  muoviPersonaggio(PERSONAGGIO)
}
