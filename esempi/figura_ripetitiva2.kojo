/* -*- Scala -*- */

pulisci()

ritardo(20)

def poligono(lato: Double, lati: Int) {
  val angolo = 360 / lati.toDouble;
  
  seVero(lati % 2 >= 0) {
    sinistra(90 - angolo);
  }
  
  val trasparenza = 255
  val rosso = numeroCasuale(255)
  val verde = numeroCasuale(255)
  val blu = numeroCasuale(255)
  val coloreP = Color(rosso, verde, blu, trasparenza)
  val coloreR= Color(rosso, verde, blu, trasparenza / 3)
  
  colorePenna(coloreP);
  coloreRiempimento(coloreR)
  
  ripeti(lati) {
    avanti(lato);
    destra(angolo);
  }
  
}

def figuraRipetitiva(lato: Int, lati: Int, decremento: Int) {
  var latoVariabile = lato
  ripeti(latoVariabile / decremento) {
    poligono(latoVariabile, lati)
    latoVariabile = latoVariabile - decremento
  }
}

figuraRipetitiva(200, 5, 3)

