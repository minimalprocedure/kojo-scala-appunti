/*
  π calculation with MonteCarlo method
*/
import scala.math.{random, pow}

def π(i: Int): Double = {
  (0 until i).foldLeft(0.0) ((acc, n) =>
    acc + (if (pow(random, 2) + pow(random, 2) <= 1.0) 1.0 else 0.0)
  ) * 4 / i
}

println(s"100 points: ${π(100)}")
println(s"1000 points: ${π(1000)}")
println(s"10000 points: ${π(10000)}")
println(s"100000 points: ${π(100000)}")
println(s"1000000 points: ${π(1000000)}")
println(s"10000000 points: ${π(10000000)}")
