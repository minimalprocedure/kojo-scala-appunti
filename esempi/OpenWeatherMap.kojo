/*
  OpenWeatherMap example.
  http://openweathermap.org
  ===============================================================================
  Libreria esterna: scalaj-http
  github: https://github.com/scalaj/scalaj-http
  maven: http://search.maven.org/#search%7Cga%7C1%7Corg.scalaj
  
  Kojo supporta il caricamente di librerie esterne in vari modi.
  Onora le variabili di ambiente CLASSPATH e KOJO_CLASSPATH oppure il caricamento da una cartella
  di nome libk che va posta nella cartella utente di Kojo:  <cartella utente>/.kojo/lite/libk
  doc: http://wiki.kogics.net/sf:add-to-classpath
*/

/* Inittialization stage, turtle, color and font */
clear()
penUp()
setPenColor(black)
invisibile()

val fontSize = 16
val fontName = "Ubuntu Mono"
val startPosition = Point(-400, 300)
jumpTo(startPosition)
setPenFont(Font(fontName, fontSize))

/* main program */
import scalaj.http._
import scala.util.parsing.json.JSON

val baseUrl = "http://api.openweathermap.org/data/2.5/weather"
val apiKey = "8c957dd5fefc4e3624c2248f2aa737d9"
val CITY = "Roma"
val COUNTRY_CODE = "IT"

type MapData = Map[String, _]
type ListOfMapData = List[Map[String, _]]

def getWeatherByCityName(key: String, city: String, country: String = ""): MapData = {
  val q = if (country.isEmpty) { s"$city" } else { s"$city,$country" }
  val response = Http(baseUrl)
    .param("q", q)
    .param("units", "metric")
    .param("APPID", key)
    .asString
  val json = JSON.parseFull(response.body)
  println(response.body)
  json match {
    case None       => Map()
    case Some(json) => json.asInstanceOf[MapData]
  }
}

def writeText(data: Any, x: Double, y: Double) {
  jumpTo(x, y);
  penDown();
  write(data)
  toString; penUp()
}

def printName(data: MapData) {
  val coord = data("coord").asInstanceOf[MapData]
  val title = s"${data("name")} lat: ${coord("lat")} lon: ${coord("lon")}"
  writeText("=============================================", position.x, position.y - fontSize)
  writeText(title, position.x, position.y - fontSize)
  writeText("=============================================", position.x, position.y - fontSize)
}

def printMapData(data: MapData, key: String) {

  def printMap(md: MapData) {
    md.foreach { tuple: (String, _) =>
      writeText(s"${tuple._1}: ${tuple._2.toString}", position.x, position.y - fontSize)
    }
  }

  writeText("=============================================", position.x, position.y - fontSize)
  writeText(key, position.x, position.y - fontSize)
  writeText("=============================================", position.x, position.y - fontSize)

  /* Warning for type erasure on JVM */
  val mapData = if (data(key).isInstanceOf[MapData]) {
    val d = data(key).asInstanceOf[MapData]
    printMap(d)
  }
  else if (data(key).isInstanceOf[String]) {
    val d = data(key).asInstanceOf[String]
    writeText(s"${key}: ${d}", position.x, position.y - fontSize)
  }
  else if (data(key).isInstanceOf[Int]) {
    val d = data(key).asInstanceOf[Int]
    writeText(s"${key}: ${d.toString}", position.x, position.y - fontSize)
  }
  else if (data(key).isInstanceOf[Double]) {
    val d = data(key).asInstanceOf[Double]
    writeText(s"${key}: ${d.toString}", position.x, position.y - fontSize)
  }
  else {
    val d = (data(key).asInstanceOf[ListOfMapData](0)).asInstanceOf[MapData]
    printMap(d)
  }

}

val responseMap = getWeatherByCityName(apiKey, CITY, COUNTRY_CODE)
printName(responseMap)
printMapData(responseMap, "coord")
printMapData(responseMap, "weather")
printMapData(responseMap, "main")
printMapData(responseMap, "wind")
printMapData(responseMap, "clouds")
printMapData(responseMap, "sys")
