/* -*- Scala -*- */

/*
 FizzBuzzWoof
 */

pulisci()
alzaPenna()

def tartaScrivi(messaggio: String) {
  avanti(20)
  abbassaPenna()
  scrivi(messaggio)
  alzaPenna()
}

def fizzBuzzWoof(da: Int, a: Int, words: List[String]) {
  for (numero <- da to a) {
    val modules = List(numero % 3, numero % 5, numero % 7)
    val messaggio =
      modules.zip(words).map(e => if (e._1 > 0) "" else e._2).mkString
    tartaScrivi(
      if (messaggio.isEmpty)
        numero.toString
      else
        messaggio)
  }
}

val words = List("Fizz", "Buzz", "Woof")

fizzBuzzWoof(1, 100, words)
