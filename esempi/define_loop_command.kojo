clear

def loop(n: Int)(block: => Unit) {
  for (l <- 1 to n) {
    block
  }
}

def loopi(n: Int)(block: Int => Unit) {
  for (l <- 1 to n) {
    block(l)
  }
}

loop(4) {
  forward(100)
  right(90)
}

forward(100)

loopi(4) { i =>
  println(i)
  forward(100)
  right(90)
}
