import javafx.application.Platform
import javafx.embed.swing.JFXPanel
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.scene.control.Button
import javafx.event.ActionEvent
import javafx.beans.binding.Bindings
import javax.swing.JFrame
import javax.swing.SwingUtilities
import javafx.scene.input.MouseEvent
import javafx.event.EventHandler

// Implicito per usare una closure come handler di evento: scala < scala 2.12
import scala.language.implicitConversions
object FXEvent2HandlerImplicits {
  implicit def mouseEvent2EventHandler(event: MouseEvent => Unit) = new EventHandler[MouseEvent] {
    override def handle(hEvent: MouseEvent): Unit = event(hEvent)
  }
}
import FXEvent2HandlerImplicits._ 

class HostFx(title: String, panel: JFXPanel) {

  val frame = new JFrame(title)
  frame.add(panel)
  frame.setSize(panel.getSize)
  frame.setLocation(100, 100)
  frame.setVisible(true);

}

class MyPanel extends JFXPanel {

  val root = new Group()
  val scene = new Scene(root, Color.ALICEBLUE)

  val text = new Text()
  text.setX(40)
  text.setY(100)
  text.setFont(new Font(25))
  text.setText("JavaFx")
  text.textProperty.bind(
    Bindings.when(text.hoverProperty())
      .then("AAAAA")
      .otherwise("BBBBB")
  )

  val button = new Button("click")
  button.setLayoutX(40)
  button.setLayoutY(140)
  button.setOnMouseClicked(
    (ev: MouseEvent) => {
      println("Click!")
    })

  root.getChildren.add(text)
  root.getChildren.add(button)

  this.setScene(scene)
  this.setSize(300, 300)

}

new HostFx("test", new MyPanel())
