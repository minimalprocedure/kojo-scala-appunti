/* -*- Scala -*- */

/*
 FizzBuzzWoof
 */

pulisci()
alzaPenna()

def tartaScrivi(messaggio: String) {
  avanti(20)
  abbassaPenna()
  scrivi(messaggio)
  alzaPenna()
}

def fizzBuzzWoof(da: Int, a: Int) {
  for (numero <- da to a) {
    val messaggio = (numero % 3, numero % 5, numero % 7) match {
      case (0, 0, 0) => "FizzBuzzWoof"
      case (0, 0, _) => "FizzBuzz"
      case (0, _, 0) => "FizzWoof"
      case (0, _, _) => "Fizz"
      case (_, 0, 0) => "BuzzWoof"
      case (_, 0, _) => "Buzz"
      case (_, _, 0) => "Woof"
      case (_, _, _) => numero.toString
    }
    tartaScrivi(messaggio)
  }
}

fizzBuzzWoof(1, 100)
