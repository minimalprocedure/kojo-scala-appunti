/* -*- Scala -*- */

/*
 FizzBuzz 2
 */

pulisci()
alzaPenna()

def tartaScrivi(messaggio: String) {
  avanti(20)
  abbassaPenna()
  scrivi(messaggio)
  alzaPenna()
}

def fizzBuzz(da: Int, a: Int) {
  for (numero <- da to a) {
    val messaggio = (numero % 3, numero % 5) match {
      case (0, 0) => "FizzBuzz"
      case (0, _) => "Fizz"
      case (_, 0) => "Buzz"
      case (_, _) => numero.toString
    }
    tartaScrivi(messaggio)
  }
}

fizzBuzz(1, 100)
