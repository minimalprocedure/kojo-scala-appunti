/* -*- Scala -*- */

//Cifrario a scostamento Vigenere/Vernam

val testo = "Caro amico ci vogliamo incontrare stasera per giocare?"
val password = "pippo"

//val testo = leggiLinea()
//val password = leggiLinea()

def cifra(testo: String, password: String): String = {
  val verme = password * (testo.length.toFloat / password.length).ceil.toInt
  testo.zip(verme).foldLeft("")((a, b) =>
    a + (b._1 ^ b._2).toChar.toString)
}

val cifrato = cifra(testo, password)
val decifrato = cifra(cifrato, password)

scriviLinea("testo: " + testo)
scriviLinea("password: " + password)
scriviLinea("testo cifrato: " + cifrato)
scriviLinea("testo decifrato: " + decifrato)

